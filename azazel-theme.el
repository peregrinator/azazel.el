;;; azazel-theme.el --- A monochromatic, bluish-red theme

;; Copyright (C) 2020 Brihadeesh S (@gitlab/peregrinator; @github/peregrinat0r)
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;; Author: Brihadeesh S <brihadeesh@protonmail.com>
;; URL: https://gitlab.com/peregrinator/azazel.el
;; Version: 0.2
;; Package-Requires: ((colorless-themes "0.1"))
;; License: MIT
;; Keywords: faces theme

;;; Commentary:
;; This is a fork of beelzebub, a nearly colourless vim theme by Stanislav Karkavin
;; https://github.com/xdefrag/vim-beelzebub;
;; Made with the `colorless-themes` macro from Thomas Letan
;; https://github.com/peregrinat0r/colourless-themes.el

;;; Code:
(require 'colourless-themes)

(deftheme azazel "A monochromatic bluish-red theme")

(colourless-themes-make azazel
                       "#262626"    ; bg
                       "#262626"    ; bg+
                       "#1c1c1c"    ; current-line
                       "#262626"    ; fade
                       "#d7875f"    ; fg
                       "#5f5faf"    ; fg+
                       "#5f5faf"    ; primary
                       "#ff5f5f"    ; red
                       "#ff5f5f"    ; orange
                       "#ffd7af"    ; yellow
                       "#87af87")   ; green

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'azazel)
(provide 'azazel-theme)
;;; azazel-theme.el ends here
