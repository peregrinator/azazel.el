# azazel

A nearly colourless theme based on Stanislaw Mnizhek's vim theme
[vim-beelzebub](https://github.com/xdefrag/vim-beelzebub). This now uses my fork
of the [`colourless-themes`](https://github.com/peregrinat0r/colorless-themes.el) 
macro.

## Screenshot(s)

*work in progress*
> ![scrot running doom-emacs in urxvt](screenshots/scrot.png)

## Installation and usage

*work in progress*

## Terminal colours

Also included is a terminal colour theme with nearly identical colours 
